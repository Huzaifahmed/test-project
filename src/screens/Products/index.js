import React, { Component } from "react";
import { View, ActivityIndicator } from "react-native";
import { Table, TableWrapper, Row, Rows } from "react-native-table-component";
import firebase from "react-native-firebase";

import ProductsStyle from "./ProductsStyle";

export default class ProductsScreen extends Component {
  state = {
    tableHead: ["Description", "Price"],
    tableData: []
  };
  componentDidMount() {
    const productsRef = firebase.database().ref("products");
    productsRef.on("value", snapshot => {
      snapshot.forEach(childSnapshot => {
        const data = childSnapshot.val();
        this.setState(prevState => ({
          tableData: [...prevState.tableData, [data.description, data.price]]
        }));
      });
    });
  }

  render() {
    const { tableData, tableHead } = this.state;
    return (
      <View style={ProductsStyle.container}>
        {!tableData.length && <ActivityIndicator color="red" />}
        <Table>
          <Row
            data={tableHead}
            flexArr={[2, 1, 1]}
            style={ProductsStyle.head}
            textStyle={ProductsStyle.text}
          />
          <TableWrapper style={ProductsStyle.wrapper}>
            <Rows
              data={tableData}
              flexArr={[2, 1, 1]}
              style={ProductsStyle.row}
              textStyle={ProductsStyle.text}
            />
          </TableWrapper>
        </Table>
      </View>
    );
  }
}
