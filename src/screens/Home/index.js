import React, { Component } from "react";
import { Button, View } from "react-native";

export default class HomeScreen extends Component {
  _onPressButton = () => {
    const { navigation } = this.props;
    const { navigate } = navigation;
    navigate("Products");
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center" }}>
        <Button onPress={this._onPressButton} title="Show Products" />
      </View>
    );
  }
}
