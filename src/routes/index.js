import {
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer,
  createMaterialTopTabNavigator
} from "react-navigation";

import HomeScreen from "../screens/Home";
import ProductsScreen from "../screens/Products";

const welcomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      header: null
    }
  },
  Products: {
    screen: ProductsScreen,
    navigationOptions: {
      header: null
    }
  }
});

const Root = createSwitchNavigator(
  {
    welcomeStack
  },
  { initialRouteName: "welcomeStack" }
);

export default createAppContainer(Root);
